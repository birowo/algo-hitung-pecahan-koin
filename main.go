// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"math/bits"
)

var pecahan = [...]uint{1000, 500, 200, 100, 50, 20, 10, 5, 1}

func hitungPecahan(x uint) (ret []uint) {
	var num [9]uint
	j := uint(0)
	for i, koin := range pecahan {
		if x < koin {
			continue
		}
		num[i], x = bits.Div(0, x, koin)
		j += num[i]
	}
	ret = make([]uint, j)
	j = uint(0)
	for i, koin := range pecahan {
		for k := num[i]; k > 0; k-- {
			ret[j] = koin
			j++
		}
	}
	return
}
func main() {
	fmt.Println("Hello, 世界")
	fmt.Println(1752, ":", hitungPecahan(1752))
	fmt.Println(1886, ":", hitungPecahan(1886))
	fmt.Println(990, ":", hitungPecahan(990))
	fmt.Println(2445, ":", hitungPecahan(2445))
	fmt.Println(4004, ":", hitungPecahan(4004))
	fmt.Println(0, ":", hitungPecahan(0))
}
